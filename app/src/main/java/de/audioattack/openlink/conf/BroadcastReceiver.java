package de.audioattack.openlink.conf;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import de.audioattack.openlink.ui.IncognitoActivity;

/**
 * Created by low012 on 20.10.17.
 */

public class BroadcastReceiver extends android.content.BroadcastReceiver {

    private static final String TAG = BroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (intent != null
                && (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())
                || Intent.ACTION_MY_PACKAGE_REPLACED.equals(intent.getAction()))) {

            addPackageRelatedReceivers(context, this);
        }

        new ComponentConfigurator(context).setEnabled(IncognitoActivity.class, RequirementsChecker.isIncognitoBowserInstalled(context));
    }

    public static void addPackageRelatedReceivers(final Context context, final BroadcastReceiver broadcastReceiver) {

        final IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            filter.addAction(Intent.ACTION_MY_PACKAGE_REPLACED);
        }

        context.getApplicationContext().registerReceiver(broadcastReceiver, filter);
    }

}
